rails_env = new_resource.environment["RAILS_ENV"]
Chef::Log.info("Precompiling assets for RAILS_ENV=#{rails_env}...")

execute "rails assets:precompile" do
  cwd release_path
  command "bundle exec rails assets:precompile"
  environment "RAILS_ENV" => rails_env
end
